FROM node:16 as build-stage

WORKDIR /app

COPY package*.json ./
RUN node --version
RUN npm install

COPY . .

RUN npm run build

# Use Nginx as the production server
FROM nginx:alpine

COPY --from=build-stage /app/build /usr/share/nginx/html

# Expose port 80 for the Nginx server
EXPOSE 80

# Start Nginx when the container runs
CMD ["nginx", "-g", "daemon off;"]