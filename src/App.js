import { useEffect } from "react";
import "./App.css";
import { useState } from "react";

function App() {
  const [data, setData] = useState(null);
  const [error, setError] = useState();

  useEffect(() => {
    try {
      fetch(`http://${window.location.hostname}:8087/api/container`, {
        method: "GET",
      })
        .then((response) => response.json())
        .then((data) => {
          setData(data.data);
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {
      setError(error);
    }
  }, []);
  return (
    <div className="App">
      <div id="particles-js"></div>
      <div className="content">
        <h1>Container Name: {data || "Loading"}</h1>
        <p>Welcome to New Tag Something</p>
        {error && <p>{error}</p>}
        {error && <p>{error}</p>}
        <button className="spacex-button">OK</button>
      </div>
    </div>
  );
}

export default App;
